import { NgModule } from '@angular/core';
import { CorDestaqueComponent } from './cor-destaque/cor-destaque';
@NgModule({
	declarations: [CorDestaqueComponent],
	imports: [],
	exports: [CorDestaqueComponent]
})
export class ComponentsModule {}
