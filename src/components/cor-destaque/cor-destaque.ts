import { Component } from '@angular/core';

/**
 * Generated class for the CorDestaqueComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'cor-destaque',
  templateUrl: 'cor-destaque.html'
})
export class CorDestaqueComponent {

  text: string;

  constructor() {
    console.log('Hello CorDestaqueComponent Component');
    this.text = 'Hello World';
  }

}
