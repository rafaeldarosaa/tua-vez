import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AmbientesProvider } from "../../providers/ambientes/ambientes";
import { Http } from '@angular/http';

/**
 * Generated class for the Ambientes page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ambientes',
  templateUrl: 'ambientes.html',
  providers: [
    AmbientesProvider  
  ]
})
export class AmbientesPage {
  public ambientes_list = new Array<any>();

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      private AmbientesProvider: AmbientesProvider,
      public http: Http
    ) {
  }

  ionViewDidLoad() {
    this.AmbientesProvider.getAmbientesList().subscribe(
      data => {
        const response = (data as any);
        this.ambientes_list = JSON.parse(response._body);
      }, error => {
        console.log(error);
      }
    )
  }

  deletaAmbiente(id_ambiente) {
    this.AmbientesProvider.delAmbiente(id_ambiente).subscribe(
      data => {
        const response = (data as any);
        console.log(response);
      }, error => {
        console.log(error);
      }
    )
  }

  paginaAddAmbiente() {
    this.navCtrl.push('AmbienteAddPage');
  }
}
