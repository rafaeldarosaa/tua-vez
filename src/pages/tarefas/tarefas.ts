import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TarefasProvider } from "../../providers/tarefas/tarefas";

/**
 * Generated class for the TarefasPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tarefas',
  templateUrl: 'tarefas.html',
  providers: [
    TarefasProvider  
  ]
})
export class TarefasPage {
  public tarefas_list = new Array<any>();

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      private TarefasProvider: TarefasProvider
    ) {
  }

  ionViewDidLoad() {
    this.TarefasProvider.getTarefasList().subscribe(
      data => {
        const response = (data as any);
        this.tarefas_list = JSON.parse(response._body);
      }, error => {
        console.log(error);
      }
    )
  }

  delTarefa(id_tarefa) {
    this.TarefasProvider.delTarefa(id_tarefa).subscribe(
      data => {
        const response = (data as any);
        console.log(response._body);
      }, error => {
        console.log(error);
      }
    )
  }

  paginaAddTarefa() {
    this.navCtrl.push('TarefaAddPage');
  }
}
