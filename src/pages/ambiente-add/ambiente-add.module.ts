import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AmbienteAddPage } from './ambiente-add';

@NgModule({
  declarations: [
    AmbienteAddPage,
  ],
  imports: [
    IonicPageModule.forChild(AmbienteAddPage),
  ],
})
export class AmbienteAddPageModule {}
