import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AmbientesProvider } from '../../providers/ambientes/ambientes';

/**
 * Generated class for the AmbienteAddPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ambiente-add',
  templateUrl: 'ambiente-add.html',
})
export class AmbienteAddPage {
  titulo;
  descricao;
  cor;

  constructor(public navCtrl: NavController, public navParams: NavParams, private ambientesProvider: AmbientesProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AmbienteAddPage');
  }

  addAmbiente() {
    this.ambientesProvider.postAmbiente(this.titulo, this.descricao, this.cor);
  }
}
