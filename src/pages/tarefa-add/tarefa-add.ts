import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TarefasProvider } from '../../providers/tarefas/tarefas';

/**
 * Generated class for the TarefaAddPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tarefa-add',
  templateUrl: 'tarefa-add.html',
})
export class TarefaAddPage {
  titulo;
  descricao;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private tarefasProvider: TarefasProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TarefaAddPage');
  }

  addTarefa() {
    return this.tarefasProvider.postTarefas(this.titulo, this.descricao);
  }
}
