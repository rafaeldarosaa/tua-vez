import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  username;
  password;

  constructor(public navCtrl: NavController, public navParams: NavParams, private authProvider: AuthProvider) {
    console.log('construtor login');
    if(localStorage.getItem('wpTuaVezTokenWP')) {
      this.navCtrl.setRoot('TabsPage');
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  iniciaLogin() {
    console.log('iniciou login');
    this.authProvider.postLogin(this.username, this.password).subscribe(data => {
      const response = (data as any);
      localStorage.setItem('wpTuaVezTokenWP', response._body);
      this.navCtrl.setRoot('HomePage');
    });
  }
}
