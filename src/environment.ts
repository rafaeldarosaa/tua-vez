export const environment = {
    site_url: 'http://localhost/rafael/tuavez-server',
    pod_ambiente: '/wp-json/wp/v2/pod_ambiente/',
    pod_task: '/wp-json/wp/v2/pod_tarefa/',
    jwt_url: '/wp-json/jwt-auth/v1/token'
}