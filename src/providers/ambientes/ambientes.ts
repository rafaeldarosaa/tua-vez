import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {environment} from '../../environment';

/*
  Generated class for the GroupsProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class AmbientesProvider {

  ambiente_api_url = environment.site_url + environment.pod_ambiente;

  constructor(public http: Http) {
    console.log('Ambientes Provider');
  }

  private getTypeUrl() {
    return this.ambiente_api_url;
  }

  getAmbientesList() {
    return this.http.get(this.getTypeUrl());
  }

  postAmbiente(titulo, descricao, cor) {
    let data = {
      title: titulo,
      amb_descricao: descricao,
      amb_cor: cor,
      status: 'publish'
    }

    let token = JSON.parse(localStorage.getItem('wpTuaVezTokenWP')).token;

    var headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    });
    const requestOptions = new RequestOptions({ headers: headers });

    return this.http.post(this.ambiente_api_url, data, requestOptions);
  }

  delAmbiente(id_ambiente) {
    let token = JSON.parse(localStorage.getItem('wpTuaVezTokenWP')).token;

    var headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    });
    const requestOptions = new RequestOptions({ headers: headers });
    
    return this.http.delete(this.ambiente_api_url + id_ambiente, requestOptions);
  }
}
