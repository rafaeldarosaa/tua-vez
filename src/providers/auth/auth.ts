import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import {environment} from '../../environment';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class AuthProvider {
  api_url = environment.site_url + environment.jwt_url;

  constructor(public http: Http) {
    console.log('Hello AuthProvider Provider');
  }

  postLogin(username, password) {
    let data = {
      username: username,
      password: password
    }

    var headers = new Headers();
    //headers.append('Accept-application', 'application/json' );
    headers.append('Content-Type', 'application/json' );
    const requestOptions = new RequestOptions({ headers: headers });

    return this.http.post(this.api_url, data, requestOptions);
  }
}