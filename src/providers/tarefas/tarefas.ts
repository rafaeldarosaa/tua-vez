import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { environment } from '../../environment';

/*
  Generated class for the TarefasProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class TarefasProvider {

  tarefa_api_url = environment.site_url + environment.pod_task;

  constructor(public http: Http) {
    console.log('Tarefas Provider');
  }

  private getTypeUrl() {
    return this.tarefa_api_url;
  }

  getTarefasList() {
    return this.http.get(this.getTypeUrl());
  }

  postTarefas(titulo, descricao) {
    let data = {
      title: titulo,
      task_desc: descricao,
      status: 'publish'
    }

    let token = JSON.parse(localStorage.getItem('wpTuaVezTokenWP')).token;

    var headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    });
    const requestOptions = new RequestOptions({ headers: headers });

    return this.http.post(this.tarefa_api_url, data, requestOptions);
  }

  delTarefa(id_ambiente) {
    let token = JSON.parse(localStorage.getItem('wpTuaVezTokenWP')).token;

    var headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    });
    const requestOptions = new RequestOptions({ headers: headers });

    return this.http.delete(this.tarefa_api_url + id_ambiente, requestOptions);
  }
}
